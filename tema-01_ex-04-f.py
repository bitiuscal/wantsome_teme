"""
tema-01_exercitiul-04 cu functii
Scrieți un program Python care să determine dacă un număr este par.
"""
# Git: un exercitiu - un commit

def get_numar(text):
    numar = float(input(text))
    if numar.is_integer():
        numar = int(numar)
    return numar

def este_par(numar):
    if numar % 2 == 0:
        return True
    return False

def main():
    numar = get_numar("introduceti un numar: ")
    print("numarul introdus este:", numar)

    par_impar = este_par(numar)
    if par_impar:
        print("numarul", numar, "este par")
    else:
        print("numarul", numar, "este impar")

main()
