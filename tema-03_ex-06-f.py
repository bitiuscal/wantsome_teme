"""
tema-03_exercitiul-06 cu functii
Tuxy vrea sa aleaga cel mai bun cercetator din tara.
Pentru a face asta a organizat alegeri in fiecare laboator si a primit rezultatele locale, 
acum are nevoie de ajutorul tau sa afle rezultatele finale.
Tuxy are un dictionar de forma:
rezultate_locale = {
'laboator iasi': {
    'Cercetator 1': 128,
    'Cercetator 2': 150
    'Cercetator 2': 94,
   ....
    }
'laboator bucuresti': {
    'Cercetator 3': 281,
    'Cercetator 4': 224
    'Cercetator 10': 412,
    ....
    }
}
El are nevoie de un dictionar care ii spune numarul total de voturi adunate de un cercetator.
"""
# Git: un exercitiu - un commit

def numara_voturi(rezultate_locale):
    dict_final = {}
    for lab in rezultate_locale:
        dict_temp = rezultate_locale[lab]
        #print(dict_temp)
        for key in dict_temp:
            if key in dict_final:
                dict_final[key] = dict_final[key] + dict_temp[key]
            else:
                dict_final[key] = dict_temp[key]
    print("rezultate finale:")
    print(dict_final)

def main():
    rezultate_locale = {
    'laboator iasi': {
        'Cercetator 1': 128,
        'Cercetator 2': 150,
        'Cercetator 2': 94
        },
    'laboator bucuresti': {
        'Cercetator 3': 281,
        'Cercetator 4': 224,
        'Cercetator 10': 412
        }
    }
    numara_voturi(rezultate_locale)

    print("\ncine e Tuxy ???\n")

main()
