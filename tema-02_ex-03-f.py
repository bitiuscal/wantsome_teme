"""
tema-02_exercitiul-03 cu functii
În laboratorul lui Tuxy toți cercetătorii au asignat un id de utilizator.
Pentru fiecare cercetător se salvează într-o listă de fiecare dată
când a deschis ușa (fie pentru a intra, fie pentru a ieși).
Tuxy suspectează că cineva rămâne tot timpul după program și ar dori
să scrie un script care să îi verifice teoria, dar nu a reușit pentru
că algoritmul său era prea costisitor pentru sistem.
Cerințe:
- Găsește cercetătorul ce stă peste program după o singură parcurgere a listei
- Găsește cercetătorul ce stă peste program după o singură parcurgere a listei
    și fără a aloca memorie suplimentară.
Notă: Există doar un singur cercetător care stă peste program
"""
# Git: un exercitiu - un commit

# prima versiune

def main():
    pontaj = [1, 1, 2, 2, 3, 4, 4]

    numara = 0
    cercetator_temp = 0
    for cercetator in pontaj:
        if cercetator == cercetator_temp:
            numara = numara + 1
        else:
            if cercetator_temp != 0:   # nu exista cercetator cu ID = 0
                if numara % 2 != 0:
                    print("cercatatorul cu ID:", cercetator_temp, "inca nu a iesit din laborator")
                    break
            cercetator_temp = cercetator
            numara = 1
    else:
        print("toti cercetatorii au iesit din laborator")

main()
