"""
tema-03_exercitiul-02 cu functii
Creati un program care combina doua dictionare in unul singur ?
Aveti grija ce se intampla cand ambele au valori pentru aceeasi cheie.
"""
# Git: un exercitiu - un commit

def main():
    d1 = {1: 1, 2: 2, 3: 3, 'k1': 4, 'k2': 5}
    d2 = {1: 11, 12: 2, 3: 13, 'k1': 'abcd', 'k2': 'xyzw'}

    new_d = d1.copy()

    for key in d2:
        if key in new_d:
            new_d[key] = (new_d[key], d2[key])
        else:
            new_d[key] = d2[key]
            
    print("dictionarul combinat:", new_d)

main()
