"""
tema-03_exercitiul-03 cu functii
Scrieti un program care numara aparitiile cuvintelor intr-un text
si le afiseaza la ecran pe 5 cele mai folosite cuvinte.
"""
# Git: un exercitiu - un commit

def text_in_list(text):
    """ transform text in list """
    text_list = text.split(sep = ' ')
    #print(text_list)
    return text_list

def count_words(text_list):
    """ count words in the text and added in a dict """
    text_dict = {}
    for item in text_list:
        if item in text_dict:
            text_dict[item] = text_dict[item] + 1
        else:
            text_dict[item] = 1
    print("dictionarul initial:", text_dict)
    return text_dict

def reverse_dict(text_dict):
    """ create a new dict where keys will be values and values will be keys """
    text_dict_count = {}
    for key in text_dict:
        if text_dict[key] in text_dict_count:
            text_dict_count[text_dict[key]].append(key)
        else:
            text_dict_count[text_dict[key]] = [key]
    print("dictionarul inversat:", text_dict_count)
    return text_dict_count

def sort_reverse_dict(text_dict_count):
    """ sort the text_dict_count """
    text_dict_sort = sorted(text_dict_count, reverse = True)
    print("dictionarul inversat sortat (doar key-ile):", text_dict_sort)
    return text_dict_sort

def create_list_top_words(text_dict_sort, text_dict_count):
    """ create a list with top words """
    text_list_top = []
    for element in text_dict_sort:
        # print(text_dict_count[element])
        if len(text_dict_count[element]) == 1:
            text_list_top.append(text_dict_count[element][0])
        else:
            for i in range(len(text_dict_count[element])):
                text_list_top.append(text_dict_count[element][i])
    return text_list_top

def main():
    text01 = (
        "abc def ghi jkl mno pqr stu vxy zwq "
        "abc mno stu zwq def abc ghi def pqr vxy zwq abc def mno zwq zwq jkl jkl jkl jkl"
        )
    #print(text01)

    text_list = text_in_list(text01)

    text_dict = count_words(text_list)

    text_dict_count = reverse_dict(text_dict)

    text_dict_sort = sort_reverse_dict(text_dict_count)

    text_list_top = create_list_top_words(text_dict_sort, text_dict_count)

    top_words = 5
    print("lista cu top words:")
    for index in range(top_words):
        print("   %d. cuvantul %s are %d aparitii" % (index + 1, text_list_top[index], text_dict[text_list_top[index]]))


main()

"""
greu exercitiul pentru incepatori, trebuie sa stii ceva programare
"""
