"""
tema-01_suplimentar cu functii
Scrieți un program Python care să îi ofere utilizatorului să ghicească un număr cuprins în intervalul 0 - 100 din maxim 7 încercări.
Notă: Pentru moment numărul poate să fie predefinit în cadrul programului. De exemplu:
secret = 67
La fiecare etapă de joc utilizatorul va trebui să propună un număr. Pentru fiecare număr primit programul va trebui să facă următoarele acțiuni:
    să verifice dacă valoarea primită este un număr în intervalul 0 - 100
        în caz contrar va afișa pe ecran un mesaj de eroare
    să verifice etapa din joc
        în cazul în care utilizatorul a epuizat cele 7 încercări, jocul se va termina și utilizatorul va primi un mesaj corespunzător.
    să compare numărul cu valoarea aleasă (cu secret):
        dacă valorile sunt egale, jucătorul a câștigat jocul
        dacă valoarea aleasă este mai mică se va afișa pe ecran: Numărul este mai mare.
        dacă valoarea aleasă este mai mare se va afișa pe ecran: Numărul este mai mic.
"""
# Git: un exercitiu - un commit

def get_numar():
    numar = -1
    while numar < 0 or numar > 100:
        numar = float(input("dati un numar secret cuprins intre 0 si 100: "))
        if numar < 0 or numar > 100:
            print("   numarul introdus nu este intre 0 si 100")

    if numar.is_integer():
        numar = int(numar)

    return numar

def ghiceste_numar(numar, incercari_maxime):
    incercari = 0
    numar_ghicit = 0
    while incercari < incercari_maxime and numar_ghicit == 0:
        incercari = incercari + 1
        numar_posibil = float(input("incercarea numarul {} - incercati sa ghiciti numarul secret: ".format(incercari)))
        if numar_posibil < 0 or numar_posibil > 100:
            print("   eroare: numarul introdus {} nu este cuprins intre 0 si 100".format(int(numar_posibil)))

        if numar == numar_posibil:
            numar_ghicit = 1
        elif numar > numar_posibil:
            print("   nu ati ghicit, numarul este mai mare")
        else:
            print("   nu ati ghicit, numarul este mai mic")

    return numar_ghicit

def main():
    numar_secret = get_numar()

    incercari_maxime = 7
    numar_ghicit = ghiceste_numar(numar_secret, incercari_maxime)

    if numar_ghicit == 1:
        print("!!! felicitari, ati ghicit numarul secret !!!")
    else:
        print("nu ati reusit sa ghiciti numarul secret din", incercari_maxime, "incercari")

main()
