"""
tema-03_exercitiul-08_criptare cu functii
Organizaţia Internaţională a Aviaţiei Civile propune un alfabet în care fiecărei litere îi este asignat un
cuvânt pentru a evita problemele în înțelegerea mesajelor critice.
Pentru a se păstra un istoric al conversațiilor s-a decis transcrierea lor conform următoarelor reguli:
    fiecare cuvânt este scris pe o singură linie
    literele din alfabet sunt separate de o virgulă
Avem nevoie de doua programe, unul de criptare si unul de decriptare care vor primi ca input textul.
Alfabetul ICAO este urmatorul:
ICAO = {
    'a': 'alfa', 'b': 'bravo', 'c': 'charlie', 'd': 'delta', 'e': 'echo',
    'f': 'foxtrot', 'g': 'golf', 'h': 'hotel', 'i': 'india', 'j': 'juliett',
    'k': 'kilo', 'l': 'lima', 'm': 'mike', 'n': 'november', 'o': 'oscar',
    'p': 'papa', 'q': 'quebec', 'r': 'romeo', 's': 'sierra', 't': 'tango',
    'u': 'uniform', 'v': 'victor', 'w': 'whiskey', 'x': 'x-ray', 'y': 'yankee',
    'z': 'zulu'
}
"""
# Git: un exercitiu - un commit

def criptare(ICAO, text_intrare):
    text_criptat = ''
    for litera in text_intrare:
        if litera in ICAO:
            #print(ICAO[litera])
            text_criptat = text_criptat + ICAO[litera] + ' '
        if litera == ' ':
            text_criptat = text_criptat + '\n'

    return text_criptat

def main():
    ICAO = {
        'a': 'alfa', 'b': 'bravo', 'c': 'charlie', 'd': 'delta', 'e': 'echo',
        'f': 'foxtrot', 'g': 'golf', 'h': 'hotel', 'i': 'india', 'j': 'juliett',
        'k': 'kilo', 'l': 'lima', 'm': 'mike', 'n': 'november', 'o': 'oscar',
        'p': 'papa', 'q': 'quebec', 'r': 'romeo', 's': 'sierra', 't': 'tango',
        'u': 'uniform', 'v': 'victor', 'w': 'whiskey', 'x': 'x-ray', 'y': 'yankee',
        'z': 'zulu'
        }
    
    text_intrare = "gigi contra baga strambe"

    text_criptat = criptare(ICAO, text_intrare)
    print("textul criptat:")
    print(text_criptat)

main()
