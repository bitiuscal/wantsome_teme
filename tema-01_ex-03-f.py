"""
tema-01_exercitiul-03 cu functii
Scrieți un program Python care să rezolve o ecuație de gradul 1.
Ecuațiile de gradul 1 sunt de forma a * x + b = c. 
Valorile pentru a, b, c vor fi primite de la utilizator, iar programul va afișa valoarea lui x.
"""
# Git: un exercitiu - un commit

def get_numar(text):
    numar = float(input(text))
    if numar.is_integer():
        numar = int(numar)
    return numar

def get_x_ecuatie(param_a, param_b, param_c):
    param_x = (param_c - param_b) / param_a
    if param_x.is_integer():
        param_x = int(param_x)
    return param_x

def main():
    print("dati valorile necunoscutelor pentru ecuatia de gradul I: a * x + b = c")

    valoare_a = get_numar("valoare a: ")
    valoare_b = get_numar("valoare b: ")
    valoare_c = get_numar("valoare c: ")

    if valoare_b > 0:
        print("ecuatia de gradul I este: {} * x + {} = {}".format(valoare_a, valoare_b, valoare_c))
    else:
        print("ecuatia de gradul I este: {} * x - {} = {}".format(valoare_a, abs(valoare_b), valoare_c))

    valoare_x = get_x_ecuatie(valoare_a, valoare_b, valoare_c)
    print("valoarea necunoscutei x este:", valoare_x)

main()
