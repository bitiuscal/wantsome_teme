"""
tema-03_exercitiul-05 cu functii
Scrieti un program care va elimina dintr-un dictionar cheile care au valori duplicate
Exemplu: {'a': 1, 'b': 1, 'c': 1, 'd': 2} -> {'d': 2}
"""
# Git: un exercitiu - un commit

def elimina_duplicate(dict_):
    """ create a new dict where keys will be values and values will be keys """
    dict_inverse = {}
    for key in dict_:
        if dict_[key] in dict_inverse:
            dict_inverse[dict_[key]].append(key)
        else:
            dict_inverse[dict_[key]] = [key]
    print("dictionarul inversat:", dict_inverse)

    new_dict = {}
    for index in dict_inverse:
        #print(dict_inverse[index])
        if len(dict_inverse[index]) == 1:
            new_dict[dict_inverse[index][0]] = index

    return new_dict

def main():
    dict_ = {'a': 1, 'b': 1, 'c': 1, 'd': 2, 'e': 3, 'f': 4, 'g': 4}

    new_dict = elimina_duplicate(dict_)
    print("dictionarul fara duplicate este:")
    print(new_dict)

main()
