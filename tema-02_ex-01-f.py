"""
tema-02_exercitiul-01 cu functii
Scrieți un program Python care să primească un text și să afișeze pe ecran în ordine alfabetică
toate cuvintele cu un număr impar de caractere și frecvența unei vocale egală cu 3.
Condiții:
    - cuvântul trebuie sa aibă număr impar de caractere;
    - cuvântul trebuie să conțină cel puțin o vocală cu numărul de apariții egal cu 3;
Cerințe:
    - trebuie să găsim toate cuvintele ce respectă condițiile de mai sus
    - trebuie să le afișăm pe ecran în ordine alfabetică
"""
# Git: un exercitiu - un commit

# vocale: a, e, i, o, u

def cauta_cuvinte(p_text):
    text_list = p_text.split()
    #print("textul:", text_list)

    cuvinte_impare = set()
    for cuvant in text_list:
        if len(cuvant) % 2 != 0:
            for vocala in ('a', 'e', 'i', 'o', 'u'):
                if cuvant.count(vocala) == 3:
                    cuvinte_impare.add(cuvant)
            
    cuvinte_impare = list(cuvinte_impare)
    cuvinte_impare.sort()

    return cuvinte_impare

def main():
    text = "xxxiiii oootttt txisjiasdiq aaabb ssaaavvveee asdlhdr"
    cuvinte_gasite = cauta_cuvinte(text)

    print("cuvintele ordonate cu numar impar de caractere si o vocala ce apare de 3 ori:")
    for cuvant in cuvinte_gasite:
        print("   cuvantul {} are {} caractere".format(cuvant, len(cuvant)))


main()
