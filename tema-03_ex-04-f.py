"""
tema-03_exercitiul-04 cu functii
Scrieti un program care citeste de la tastatura un numar N si construieste un dictionar cu urmatoarele proprietati:
dictionarul are N chei fiecare cheie este un numar de la [0, N)
valoarea unei chei (k) este o lista de forma:
[<True/False> daca numarul este prim sau nu, <suma numerelor pana la k, inclusiv k>]
"""
# Git: un exercitiu - un commit

def get_numar(text):
    numar = float(input(text))
    if numar.is_integer():
        numar = int(numar)
    return numar

def construieste_numar_dictionar(numar):
    numar_dict = {}
    for key in range(numar):
        # verifica daca key este prim sau nu
        numar_dict[key] = [True]
        prim = 1
        numere = range(2, int(key / 2) + 1)
        for i in numere:
            if key % i == 0:
                prim = 0
                break
        if prim == 0:
            numar_dict[key] = [False]

        # calculeaza suma numerelor pana la k, inclusiv k
        suma = 0
        for i in range(key + 1):
            suma = suma + i
        numar_dict[key].append(suma)
    return numar_dict

def afiseaza_numar_dict(numar_dict):
    print("numarul dictionar este:")
    for key in numar_dict:
        print('numar: {}, prim: {}, suma pana la el (inclusiv): {}'.format(key, numar_dict[key][0], numar_dict[key][1]))

def main():
    numar = get_numar("dati un numar: ")
    print("numarul introdus este:", numar)

    numar_dict = construieste_numar_dictionar(numar)

    afiseaza_numar_dict(numar_dict)

main()

