"""
tema-02_exercitiul-02 cu functii
Se primesc două valori numerice (ca număr sau ca șir de caractere la alegere).
Scrieți un program Python care să verifice dacă fiecare cifră/caracter din prima valoare
se regăsește și în a doua valoare (și vice-versa).
"""
# Git: un exercitiu - un commit

def este_numar(text):
    if not text:
        return False

    if text.startswith('0'):
        return False

    for caracter in text:
        if not ord('0') <= ord(caracter) <= ord('9'):
            return False

    return True

def get_numar(text):
    while True:
        numar = input(text)
        if not este_numar(numar):
            print('   valoare invalida')
            print('   incearca din nou')
            continue
        break
    return numar
   
    
def main():
    numar1 = get_numar("prima valoare: ")
    #print(numar1, type(numar1))
    numar2 = get_numar("a doua valoare: ")
    #print(numar2, type(numar1))

    numara_aparitii = 0
    for cifra in numar1:
        if numar2.count(cifra) != 0:
            numara_aparitii = numara_aparitii + 1
            print("cifra {} din primul numar apare si in al doilea numar".format(cifra))
        else:
            print("cifra {} din primul numar NU apare si in al doilea numar".format(cifra))
    print("   =>", numara_aparitii, "caractere din primul numar apar si in al doilea numar")

    numara_aparitii = 0
    for cifra in numar2:
        if numar1.count(cifra) != 0:
            numara_aparitii = numara_aparitii + 1
            print("cifra {} din al doilea numar apare si in primul numar".format(cifra))
        else:
            print("cifra {} din al doilea numar NU apare si in primul numar".format(cifra))
    print("   =>", numara_aparitii, "caractere din al doilea numar apar si in primul numar")

main()
