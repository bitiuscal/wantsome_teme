"""
tema-01_exercitiul-05 cu functii
Scrieți un program Python care să verifice dacă un număr este prim.
Un număr este prim dacă acesta are doar doi divizori (1 și pe el însuși).
O să numim un divizor un număr la care numărul nostru se împarte exact,
sau cu alte cuvinte restul împărțirii numărului la divizor este 0.
"""
# Git: un exercitiu - un commit

def get_numar():
    numar = -1
    while numar < 1 or numar > 100000:
        numar = float(input("introduceti un numar cuprins intre 1 si 100.000: "))
        if numar < 1 or numar > 100000:
            print("   numarul introdus nu este intre 1 si 100.000")

    if numar.is_integer():
        numar = int(numar)

    return numar

def este_prim(numar):
    numere = range(2, int(numar / 2) + 1)
    divizori = []
    for i in numere:
        if numar % i == 0:
            divizori.append(i)

    return divizori
    
def main():
    numar = get_numar()
    print("numarul introdus este:", numar)

    numar_divizori = este_prim(numar)
    if len(numar_divizori) == 0:
        print("numarul introdus este prim")
    else:
        print("numarul introdus nu este prim")
        print("divizori:", numar_divizori)

main()
