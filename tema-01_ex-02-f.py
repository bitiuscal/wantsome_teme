"""
tema-01_exercitiul-02 cu functii
Scrieți un program Python care să determine valoarea maximă și valoarea minimă dintre 3 valori primite de la tastatură.
"""
# Git: un exercitiu - un commit

def get_numar(text):
    val = input(text)
    return val

def get_max(val_01, val_02, val_03):
    val_max = max(val_01, val_02, val_03)
    return val_max

def get_min(val_01, val_02, val_03):
    val_min = min(val_01, val_02, val_03)
    return val_min

def main():
    valoare_01 = get_numar("prima valoare: ")
    valoare_02 = get_numar("a doua valoare: ")
    valoare_03 = get_numar("a treia valoare: ")

    valoare_max = get_max(valoare_01, valoare_02, valoare_03)
    print("valoare maxima este: {}".format(valoare_max))

    valoare_min = get_min(valoare_01, valoare_02, valoare_03)
    print("valoare minima este: {}".format(valoare_min))

main()
