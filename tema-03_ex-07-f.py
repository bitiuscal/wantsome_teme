"""
tema-03_exercitiul-07 cu functii
Tuxy trebuie sa cumpere echipamente pentru laborator. 
A primit doua dictionare, unul cu echipamentele ce trebuie cumparate si cantiatea lor,
si unul cu preturile echipamentelor (pentru o singura unitate).
Ar vrea sa stie care e costul total.

pret_pe_unitate = {
    'monitor': 300,
    'scaun': 250,
    'unitate_de_calcul: 1500,
    'unitate_grafica': 8000
}
necesar_pentru_laborator = {
    'scaun': 4,
    'unitate_de_calcul': 200,
    'unitate_grafica': 50
}
"""
# Git: un exercitiu - un commit

def calculeaza_pret(pret_pe_unitate, necesar_pentru_laborator):
    pret_total = 0
    for item in necesar_pentru_laborator:
        #print(item)
        if item in pret_pe_unitate:
            cost = necesar_pentru_laborator[item] * pret_pe_unitate[item]
            print("{} - {} bucati, cost: {}".format(item, necesar_pentru_laborator[item], cost))
            pret_total = pret_total + cost
        else:
            print("pentru echipamentul", item, "nu este un pret de achizitie")
        
    return pret_total

def main():
    pret_pe_unitate = {
        'monitor': 300,
        'scaun': 250,
        'unitate_de_calcul': 1500,
        'unitate_grafica': 8000
    }

    necesar_pentru_laborator = {
        'scaun': 4,
        'unitate_de_calcul': 200,
        'unitate_grafica': 50,
        'birou': 10
    }

    pret_total = calculeaza_pret(pret_pe_unitate, necesar_pentru_laborator)
    print("pret total:", pret_total)

    print("\npana la urma, cine e acest Tuxy ???\n")

main()
