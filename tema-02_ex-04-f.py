"""
tema-02_exercitiul-04 cu functii
Tuxy dorește să împlementeze un nou paint pentru consolă.
În timpul dezvoltării proiectului s-a izbit de o problemă pe care nu o poate rezolva singur
și a apelat la ajutorul tău.
Aplicația ține un istoric al tuturor mișcărilor pe care le-a făcut utilizatorul într-o listă.

Exemplu de date de intrare:
istoric = [
    'STÂNGA', 2,
    'JOS', 2,
    'DREAPTA', 5,
]

Fișierul de mai sus ne spune că utilizatorul a mutat cursorul 2 căsuțe la stânga după care 2 căsuțe în jos,
iar ultima acțiune a fost să poziționeze cursorul cu 5 căsuțe în dreapta față de ultima poziție.
El dorește un utilitar care să îi spună care este distanța dintre punctul de origine (0, 0)
și poziția curentă a cursorului.
"""
# Git: un exercitiu - un commit

import math

def main():
    # STÂNGA, DREAPTA, SUS, JOS
    istoric = [
        'STÂNGA', 2,
        'JOS', 2,
        'DREAPTA', 5,
        'SUS', 10,
        'STÂNGA', 7,
        'JOS', 5
    ]

    pozitie = [0, 0]
    for i in istoric:
        if i == 'STÂNGA':
            deplasare = 'STÂNGA'
        elif i == 'DREAPTA':
            deplasare = 'DREAPTA'
        elif i == 'SUS':
            deplasare = 'SUS'
        elif i == 'JOS':
            deplasare = 'JOS'
        else:
            if deplasare == 'STÂNGA':
                pozitie[0] = pozitie[0] - i
            elif deplasare == 'DREAPTA':
                pozitie[0] = pozitie[0] + i
            elif deplasare == 'SUS':
                pozitie[1] = pozitie[1] + i
            elif deplasare == 'JOS':
                pozitie[1] = pozitie[1] - i

    print("pozitia curenta este:", pozitie)

    c1 = abs(pozitie[0])
    c2 = abs(pozitie[1])
    o1 = (c1 ** 2) + (c2 ** 2)
    distanta = math.sqrt(o1)
    print("distanta fata de originea (0, 0) este radical de", o1, "adica:", distanta)

main()
