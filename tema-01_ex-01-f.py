"""
tema-01_exercitiul-01 cu functii
Scrieți un program Python care să interschimbe două valori existente în program.
"""
# Git: un exercitiu - un commit

def get_numar(text):
    variabila = input(text)
    return variabila

def schimba_valori(var_01, var_02):
    var_temp = var_01
    var_01 = var_02
    var_02 = var_temp
    return (var_01, var_02)

def afiseaza_valoare(text, valoare):
    print(text, valoare)

def main():
    variabila_01 = get_numar("prima valoare: ")
    variabila_02 = get_numar("a doua valoare: ")

    variabila_01, variabila_02 = schimba_valori(variabila_01, variabila_02)

    afiseaza_valoare("prima valoare:", variabila_01)
    afiseaza_valoare("a doua valoare:", variabila_02)

main()
