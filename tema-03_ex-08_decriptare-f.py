"""
tema-03_exercitiul-08_decriptare cu functii
Organizaţia Internaţională a Aviaţiei Civile propune un alfabet în care fiecărei litere îi este asignat un
cuvânt pentru a evita problemele în înțelegerea mesajelor critice.
Pentru a se păstra un istoric al conversațiilor s-a decis transcrierea lor conform următoarelor reguli:
    fiecare cuvânt este scris pe o singură linie
    literele din alfabet sunt separate de o virgulă
Avem nevoie de doua programe, unul de criptare si unul de decriptare care vor primi ca input textul.
Alfabetul ICAO este urmatorul:
ICAO = {
    'a': 'alfa', 'b': 'bravo', 'c': 'charlie', 'd': 'delta', 'e': 'echo',
    'f': 'foxtrot', 'g': 'golf', 'h': 'hotel', 'i': 'india', 'j': 'juliett',
    'k': 'kilo', 'l': 'lima', 'm': 'mike', 'n': 'november', 'o': 'oscar',
    'p': 'papa', 'q': 'quebec', 'r': 'romeo', 's': 'sierra', 't': 'tango',
    'u': 'uniform', 'v': 'victor', 'w': 'whiskey', 'x': 'x-ray', 'y': 'yankee',
    'z': 'zulu'
}
"""
# Git: un exercitiu - un commit

def decriptare(ICAO, text_intrare):
    text_intrare_list = text_intrare.split(sep = '\n')
    #print(text_intrare_list)

    text_decriptat = ''
    for fraza in text_intrare_list:
        while fraza.startswith(' '):
            fraza = fraza[1:]
        while fraza.endswith(' '):
            fraza = fraza[:len(fraza) - 1]

        if len(fraza) != 0:
            #print(fraza)
            fraza_list = fraza.split(sep = ' ')
            #print(fraza_list)
            for cuvant in fraza_list:
                text_decriptat = text_decriptat + cuvant[0]

            text_decriptat = text_decriptat + ' '

    return text_decriptat

def main():
    ICAO = {
        'a': 'alfa', 'b': 'bravo', 'c': 'charlie', 'd': 'delta', 'e': 'echo',
        'f': 'foxtrot', 'g': 'golf', 'h': 'hotel', 'i': 'india', 'j': 'juliett',
        'k': 'kilo', 'l': 'lima', 'm': 'mike', 'n': 'november', 'o': 'oscar',
        'p': 'papa', 'q': 'quebec', 'r': 'romeo', 's': 'sierra', 't': 'tango',
        'u': 'uniform', 'v': 'victor', 'w': 'whiskey', 'x': 'x-ray', 'y': 'yankee',
        'z': 'zulu'
    }

    text_intrare = '''
golf india golf india 
charlie oscar november tango romeo alfa 
bravo alfa golf alfa 
sierra tango romeo alfa mike bravo echo
'''
    #print(text_intrare)

    text_decriptat = decriptare(ICAO, text_intrare)
    print("textul decriptat:")
    print(text_decriptat)
    
main()
