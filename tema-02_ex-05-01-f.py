"""
tema-02_exercitiul-05_metoda-01 cu functii
Scrieți un program Python care să determine dacă într-o listă există duplicate.
Cerințe:
    nu se poate folosi nici o funcție built-in
    problema trebuie rezolvată în cel puțin 3 moduri
"""
# Git: un exercitiu - un commit

lista = [0, 'a', 1, 'b', 2, 'c', 3, 'd', 4, 'e', 5, 'f', 'a', 0, 'd']

i = 0
for element1 in lista:
    duplicat = 1
    j = 0
    for element2 in lista:
        if j > i and element2 == element1:
            duplicat = duplicat + 1
        j = j + 1
    if duplicat >= 2:
        print("elementul", element1, "apare de 2 ori")
    i = i + 1

