"""
tema-03_exercitiul-01 cu functii
Implementati transformarea unei liste de tuple intr-un dictionar fara a folosi dict. Exemplu:
[("prima valoare", 1), ("a doua valoare", 2)] -> {'prima valoare': 1, 'a doua valoare': 2}
"""
# Git: un exercitiu - un commit

def main():
    list = [("prima valoare", 1), ("a doua valoare", 2)]

    dict = {}
    for item in list:
        dict[item[0]] = item[1]

    print("dictionarul:", dict)

main()
