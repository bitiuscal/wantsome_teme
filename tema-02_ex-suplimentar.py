"""
tema-02_exercitiul-suplimentar
Tuxy dorește să împlementeze un nou paint pentru consolă.
În timpul dezvoltării proiectului s-a izbit de o problemă pe care nu o poate rezolva singur
și a apelat la ajutorul tău.
El dorește să adauge o unealtă care să permită umplerea unei forme închise.

Exemplu:
Pornim de la imaginea inițială reprezentată mai jos, trebuie să umplem formele în care se află "x":

  |-----*------|          |******------|         |************|
  |--x--*------|          |******------|         |************|
  |******------|  ----->  |******------|  -----> |************|
  |-----******-|          |-----******-|         |-----*******|
  |-----*---*--|          |-----*---*--|         |-----*---***|
  |-----*---*-x|          |-----*---*--|         |-----*---***|
"""
# Git: un exercitiu - un commit

def afisea_imagine(imagine):
    for linie in imagine:
        for caracter in linie:
            print(caracter, end = ' ')
        print(' ')
    input('Press any key ...')

def este_gol(imag, punct):
    rand, coloana = punct
    print(rand, coloana)
    return imag[rand][coloana] == '-'

def umple_punct(imag, punct):
    rand, coloana = punct
    imag[rand][coloana] = '*'

def pozitia(pozitie, punct):
    # sus: rand -1, coloana +0
    # jos: rand +1, coloana +0
    # stanga: rand +0, coloana -1
    # dreapta: rand: +0, coloana +1

    if pozitie == 'SUS':
        rand = -1
        coloana = 0
    elif pozitie == 'JOS':
        rand = 1
        coloana = 0
    elif pozitie == 'STANGA':
        rand = 0
        coloana = -1
    elif pozitie == 'DREAPTA':
        rand = 0
        coloana = 1
    else:
        rand = 0
        coloana = 1
    return(punct[0] + rand, punct[1] + coloana)

def este_valid(imagine, punct):
    rand, coloana = punct
    if rand < 0 or rand > len(imagine):
        return False

    if coloana < 0 or coloana > len(imagine[0]):
        return False

    return True

def umple_forma(imagine, punct):
    """Funcția primește reprezentarea imaginii și coordonatele unui punct.
    În cazul în care punctul se află într-o formă închisă trebuie să
    umple forma respectivă cu caracterul "*"
    """

    de_umplut = [punct]
    while de_umplut:
        afisea_imagine(imagine)
        print('De verificat:', de_umplut)
        punctul_curent = de_umplut.pop(0)
        print('Verificare punct:', punctul_curent)
        if este_gol(imagine, punctul_curent):
            umple_punct(imagine, punctul_curent)

        coordonate = (
            pozitia('SUS', punctul_curent),
            pozitia('JOS', punctul_curent),
            pozitia('STANGA', punctul_curent),
            pozitia('DREAPTA', punctul_curent),
            )
        #print(coordonate)

        for punct_vecin in coordonate:
            if este_valid(imagine, punct_vecin)and este_gol(imagine, punct_vecin):
                print('Adaugam in de umplut', punct_vecin)
                de_umplut.append(punct_vecin)

def main():
    """  Main function docstring """
    imaginea = [
        ["-", "-", "-", "-", "-", "*", "-", "-", "-", "-", "-", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "-", "-", "-", "-", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "-", "-", "-", "-", "-"],
        ["*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "*", "-", "-", "*", "-"],
        ["-", "-", "-", "-", "-", "*", "-", "*", "-", "-", "*", "-"],
    ]
    #umple_forma(imaginea, (1, 3))
    umple_forma(imaginea, (4, 11))


if __name__ == "__main__":
    main()
